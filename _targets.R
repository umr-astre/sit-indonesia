library(targets)
library(tarchetypes)
source("src/functions.R")
options(tidyverse.quiet = TRUE)

## Uncomment below to use local multicore computing
## when running tar_make_clustermq().
# options(clustermq.scheduler = "multicore")

# Uncomment below to deploy targets to parallel jobs
# on a Sun Grid Engine cluster when running tar_make_clustermq().
# options(clustermq.scheduler = "sge", clustermq.template = "sge.tmpl")


tar_option_set(
  packages = c(
    "bayesplot",
    "brms",
    "formattable",
    "furrr",
    "ggdist",
    "ggiraph",
    "here",
    "hrbrthemes",
    "janitor",
    "kableExtra",
    "knitr",
    "mgcv",
    "readxl",
    "tidyxl",
    "tidyverse",
    "unpivotr"
  )
)

params <- list(
  ## Day of the week where adults surveys generally take place
  ## This is a guess. I have no real data about this.
  weekday_adult_surveys = "Sunday",
  weekday_egg_surveys = "Sunday"
)

## Interactive workflow
# pacman::p_load(char = tar_option_get("packages"))
# tar_load_everything()


## Define the pipeline. A pipeline is just a list of targets.
list(

  # SOURCE DATA ---------------------------------------------------

  tar_file_read(
    adult_surveillance_2_control_site_cells,
    "data/Adult surveillance_2.xlsx",
    tidyxl::xlsx_cells(!!.x, sheets = "Control Site 1")
  ),


  tar_file_read(
    adult_surveillance_2_release_site_cells,
    "data/Adult surveillance_2.xlsx",
    tidyxl::xlsx_cells(!!.x, sheets = "Release Site")
  ),

  # Weekly eggs count from 30 ovitraps in release and control sites.
  tar_file_read(
    egg_counts,
    "data/No of Eggs.xlsx",
    read_excel(!!.x, sheet = "Total", skip = 1)
  ),

  # Weekly ovitrap density index (ODI). ODI was calculated by dividing total
  # number of eggs by positive ovitrap
  tar_file_read(
    egg_odi,
    "data/No of Eggs.xlsx",
    read_excel(!!.x, sheet = "ODI")
  ),

  # Weekly ovitrap index (OI). OI was percentage of positive ovitrap from the
  # total ovitrap inspected.
  tar_file_read(
    egg_oi,
    "data/No of Eggs.xlsx",
    read_excel(!!.x, sheet = "OI")
  ),

  ## Egg surveys
  tar_file_read(
    hatching_rate_cells,
    "data/Hatching Rate.xlsx",
    tidyxl::xlsx_cells(!!.x, sheets = NA) |>
      filter(
        !is_blank,          # Remove blank cells
        is.na(formula),     # Remove calculated cells
        sheet != "Summary", # Remove calculated sheet
        row < 135           # Remove calculations below the data
      )
  )
  ,

  ## Source: Pilot Trial Population Suppression protocol
  ## surface areas for RW 11 and RW 13 reported by personal communication.
  tar_target(
    suppression_trial_sites,
    tibble(
      site = fct_inorder(c("RW 1", "RW 4", "RW 11", "RW 13")),
      type = factor(c("Control", "Release")[c(1, 2, 1, 1)]),
      surface_ha = c(5.44, 5.42, 6.4, 8.88),
      n_ovitraps = rep(30L, 4),
      n_adult_traps = c(10, 10, 0, 0)
    )
  )
  ,

  ## Released males
  ## Density is computed as total/5.42. Neglecting mortality.
  ## Units are Males/ha.
  tar_file_read(
    released_males,
    "data/No of male released.xlsx",
    read_excel(
      !!.x,
      range = "A2:H13",
      col_names = c(
        "false_date", paste0("prod_day_", 1:3), "total",
        "irradiation_dose", "density_males_per_ha", "mortality"
      ),
      na = "n.a."
    ) |>
      ## The date, written as Month-week was misinterpreted by Excel
      ## as Month-year. Thus, Oct-05 was parsed as 2005-10-01. Fix.
      mutate(
        month = months(false_date),
        week_of_month = as.POSIXlt(false_date)$year + 1900L - 2000L,
        year = ifelse(.data$month %in% month.name[6:12], 2021L, 2022L),
        date = get_release_dates(year, month, week_of_month, day = "Wednesday")
      ) |>
      ## Recompute density from real data
      mutate(
        density_males_per_ha = total /
          suppression_site_surface(
            type = "Release", data = suppression_trial_sites
          ),
        across(.cols = c(prod_day_1:total, mortality), as.integer)
      ) |>
      select(
        date, prod_day_1:mortality
      )
  )
  ,

  # DERIVED DATA --------------------------------------------------

  tar_target(
    intervention_period,
    list(
      geom_rect(
        data = data.frame(
          xmin = min(released_males$date),
          xmax = max(released_males$date)
        ),
        aes(xmin = xmin, xmax = xmax, ymin = -Inf, ymax = Inf),
        fill = 'darkorange',
        alpha = 0.3,
        inherit.aes = FALSE
      ),
      annotate(
        "text",
        x = mean(range(released_males$date)),
        y = Inf,
        label = "Intervention period",
        colour = 'darkgrey',
        vjust = -0.1,
        size = 2.5
      ),
      coord_cartesian(clip = 'off')
    )
  ),

  tar_target(
    adult_survey,
    bind_rows(
      tidy_adult_survey(adult_surveillance_2_control_site_cells),
      tidy_adult_survey(adult_surveillance_2_release_site_cells)
    ) |>
      mutate(
        site_label = fct_recode(
          site,
          `Control Site (RW 1)` = "Control Site 1",
          `Release Site (RW 4)` = "Release Site"
        ),
        site = fct_relabel(site_label, ~gsub(".*\\((RW \\d)\\)$", "\\1", .)),
        date = weekofmonth2date(
          year, month, week_of_month = week, day = "Sunday"
        ),
        sex = fct_recode(sex, Female = "♀", Male = "♂")
      )
  )
  ,

  ## Process small multiples
  ## Total counts of eggs performed at the lab after some processing which
  ## caused loss of eggs. Not to match with counts from the field.
  tar_target(
    egg_hatchings,
    tidy_egg_survey(hatching_rate_cells)
  )
  ,

  ## Add variable site-type and intervention period to egg hatchings,
  ## remove observations with missing values
  ## add unique trap identifier across sites
  tar_target(
    egg_hatchings_site_period,
    egg_hatchings |>
      filter(!is.na(total)) |>
      left_join(
        suppression_trial_sites |> select(site, site_type = type),
        by = 'site'
      ) |>
      mutate(
        intervention = date_within_intervention(
          date,
          period = range(released_males$date),
          shift_days = 14
        ),
        site = fct_relabel(
          site,
          ~ gsub("\\s(\\d)$", " 0\\1", .)
        ),
        trap = site:factor(ovitrap_id)
      )
  )
  ,


  tar_target(
    permuted_summary_egg_catches,
    seq.int(2e3) |>
      map_dfr(
        ~ summarise_ovitrap_catches(
          mutate(
            egg_hatchings_site_period,
            ovitrap_id = sample(ovitrap_id)
          )
        ),
        .id = "rep"
      ) |>
      group_by(site, order) |>
      summarise(
        q025 = quantile(percent_eggs, prob = .025),
        q500 = quantile(percent_eggs, prob = .500),
        q975 = quantile(percent_eggs, prob = .975),
        .groups = 'drop'
      )
  )
  ,

  ## Total counts of eggs on the field, across traps.
  ## We don't have individual counts per trap.
  tar_target(
    eggs_tidy,
    map(
      list(field_count = egg_counts, odi = egg_odi, oi = egg_oi),
      ~ mutate(
        .,
        ## Week number progresses sequentially until week 52 of 2021, after
        ## which it starts over from week 1 of 2022. Here I identify the row
        ## where this occurs as the only negative difference between consecutive
        ## rows.
        year = if_else(
          row_number() <= which(diff(Week) < 0),
          2021L,
          2022L
        )
      )
    ) |>
      bind_rows(.id = "variable") |>
      pivot_longer(
        cols = starts_with("RW"),
        names_to = "site",
        names_transform = list(
          site = ~factor(., levels = levels(suppression_trial_sites$site))
        ),
        values_to = "value"
      ) |>
      janitor::clean_names() |>
      mutate(week = as.integer(week)) |>
      ## Compute a provisional survey date if only to sort time correctly
      mutate(
        date = weekofmonth2date(
          year, month = "January", week_of_month = week, day = "Sunday"
        )
      ) |>
      ## This week number is the custom week count running from
      ## Wednesday to Tuesday.
      rename(week_custom = week)
  )
  ,


  tar_target(
    hatching_rates,
    egg_hatchings |>
      left_join(
        suppression_trial_sites |> select(site, site_type = type),
        by = 'site'
      ) |>
      mutate(
        day = as.integer(date - min(date)),
        hatching_rate = hatched / total
      )
  )
  ,

  # QUALITY CHECKS ----------------------------------------------------------

  ## The density of released males is correctly computed.
  tar_target(
    check_density_released_males,
    released_males |>
      mutate(
        dens = total / suppression_site_surface(
          type = "Release", data = suppression_trial_sites
        ),
        check = identical(density_males_per_ha, dens)
      ) |>
      pull(check) |>
      all()
  )
  ,

  ## The counts of hatched and unhatched eggs add up to the reported total
  tar_target(
    check_total_hatchings,
    egg_hatchings |>
      mutate(
        calc_total = unhatched + hatched,
        check = identical(total, calc_total)
      ) |>
      pull(check) |>
      all()
  )
  ,


  # PARAMETERS ----------------------------------------------------


  # DESCRIPTION ---------------------------------------------------


  # MODELS --------------------------------------------------------


  #### Hatching rates ####


  tar_target(
    induced_sterility_table,
    is_table_period(egg_hatchings_site_period) |>
      left_join(
        is_ci_period(is_boot),
        by = "Period"
      )
  )
  ,

  tar_target(
    is_model,
    brms::brm(
      hatched | trials(total) ~ site_type*intervention + (1 | trap),
      data = egg_hatchings_site_period,
      family = binomial(),
      iter = 4e3
    )
  )
  ,


  tar_target(
    is_boot,
    induced_sterility_boot(egg_hatchings_site_period)
  )
  ,

  tar_target(
    induced_sterility_samples,
    tidybayes::epred_draws(
      is_model,
      newdata = egg_hatchings_site_period |>
        select(site_type, intervention) |>
        unique() |>
        add_column(total = 1),
      ndraws = 1e3,
      re_formula = NA
    )
  )
  ,

  tar_target(
    hatching_rates_conditional_samples,
    tidybayes::epred_draws(
      is_model,
      newdata = egg_hatchings_site_period |>
        select(site_type, site, trap, ovitrap_id, intervention) |>
        ## During the intervention period
        filter(intervention) |>
        unique() |>
        add_column(total = 1),
      ndraws = 1e3
    )
  )
  ,



  #### Egg density ####

  tar_target(
    egg_density_table,
    egg_hatchings_site_period |>
      group_by(site_type, intervention) |>
      summarise(
        dens = mean(total),
        .groups = 'drop'
      ) |>
      mutate(
        Period = ifelse(intervention, "Intervention", "Off-intervention")
      ) |>
      pivot_wider(
        id_cols = Period,
        names_from = site_type,
        names_prefix = "Dens_",
        values_from = dens
      ) |>
      mutate(
        Relative_reduction = 100 * (1 - Dens_Release / Dens_Control)
      )
  )
  ,


  # tar_target(
  #   ed_model,
  #   brms::brm(
  #     y ~ site*intervention + (1 | trap),
  #     data = egg_hatchings_site_period |>
  #       mutate(y = log(total))
  #   )
  # )
  # ,

  tar_target(
    ed_model,
    brms::brm(
      y ~ site*intervention + (1 | week) + (1 | trap),
      data = egg_hatchings_site_period |>
        mutate(y = log(total))
    )
  )
  ,


  tar_target(
    egg_density_samples,
    tidybayes::epred_draws(
      ed_model,
      newdata = egg_hatchings_site_period |>
        select(site, site_type, intervention) |>
        unique(),
      ndraws = 1e3,
      re_formula = NA
    ) |>
      ## Back-transform the draws from the expected outcome
      mutate(
        .epred = exp(.epred)
      )
  )
  ,


  tar_target(
    egg_density_conditional_samples,
    tidybayes::epred_draws(
      ed_model,
      newdata = egg_hatchings_site_period |>
        select(site_type, site, trap, week, ovitrap_id, intervention) |>
        ## During the intervention period
        filter(intervention) |>
        unique(),
      ndraws = 1e3
    ) |>
      ## Back-transform the draws from the expected outcome
      mutate(
        .epred = exp(.epred)
      )
  )
  ,

  # DIAGNOSIS -----------------------------------------------------

  tar_target(
    is_model_pp_check,
    bayesplot::ppc_violin_grouped(
      egg_hatchings_site_period$hatched,
      brms::posterior_predict(is_model, ndraws = 1e3),
      group = egg_hatchings_site_period$site_type
    )
  )
  ,

  tar_target(
    is_model_pp_check_2,
    bayesplot::ppc_dens_overlay_grouped(
      egg_hatchings_site_period$hatched,
      brms::posterior_predict(is_model, ndraws = 1e2),
      group = with(egg_hatchings_site_period, site:factor(intervention))
    )
  )
  ,

  tar_target(
    ed_model_pp_check,
    bayesplot::ppc_dens_overlay_grouped(
      log(egg_hatchings_site_period$total),
      brms::posterior_predict(ed_model, ndraws = 1e2),
      group = with(egg_hatchings_site_period, site:factor(intervention))
    )
  )
  ,


  tar_target(
    conditional_hatching_rates,
    hatching_rates_conditional_samples |>
      ungroup() |>
      mutate(
        trap = fct_reorder(trap, .epred)
      ) |>
      ggplot() +
      aes(.epred, trap, colour = site_type) +
      stat_pointinterval(show.legend = FALSE) +
      labs(
        x = "Hatching rate",
        y = "Site and trap"
      ) +
      scale_colour_manual(
        values = c(Control = "darkgrey", Release = "darkorange")
      ) +
      facet_grid(site_type ~., scales = 'free_y', space = 'free_y') +
      theme(
        axis.text.y = element_text(size = 7, family = "mono")
      )
  )
  ,

  tar_target(
    conditional_densities,
    egg_density_conditional_samples |>
      ungroup() |>
      mutate(
        trap = fct_reorder(trap, .epred)
      ) |>
      ggplot() +
      aes(.epred, trap, colour = site_type) +
      stat_pointinterval(show.legend = FALSE) +
      labs(
        x = "Egg density",
        y = "Site and trap"
      ) +
      scale_colour_manual(
        values = c(Control = "darkgrey", Release = "darkorange")
      ) +
      facet_grid(site_type ~., scales = 'free_y', space = 'free_y') +
      theme(
        axis.text.y = element_text(size = 7, family = "mono")
      )
  )
  ,

# REPORTS -------------------------------------------------------

  tar_render(
  	report_html,
    "src/pilot_suppression_trial.Rmd",
    output_dir = "public",  # https://github.com/ropensci/drake/issues/742
    output_format =
      rmdformats::readthedown(
        dev = "CairoPNG",
        toc_depth = 3,
        lightbox = T,
        gallery = T,
        use_bookdown = T,
        number_sections = T),
    output_file = "public/pilot_suppression_trial.html",
      quiet = FALSE
  )
  ,

  tar_render(
  	report_pdf,
    "src/pilot_suppression_trial.Rmd",
    output_dir = "reports",  # https://github.com/ropensci/drake/issues/742
    output_format =
      bookdown::pdf_document2(
        dev = "pdf",
        includes = list(
          in_header = "preamble.tex",
          before_body = "before_body.tex"
        ),
        template = NULL,
        toc = T,
        toc_depth = 3,
        number_sections = T,
        latex_engine = "xelatex",
        keep_tex = FALSE
      ),
    output_file = "reports/pilot_suppression_trial.pdf",
    quiet = FALSE
  )

)

