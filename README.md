# SIT Indonesia

Facundo Muñoz \
January 04, 2023

Analysis of Sterile-Insect Technique (SIT) Mark-Release-Recapture (MRR) and
pilot population-suppression trial data from Indonesia.

Main objective:

-  Determine whether the release numbers were sufficient to induce 80% sterility
in the wild _Aedes_ aegypti population (egg hatch rates, induced sterility,
number of eggs, competitiveness (with and without bootstrapping method) of the
pilot vs control sites in Indonesia.


## Reports

- [Pilot trial for population suppression](https://umr-astre.pages.mia.inra.fr/sit-indonesia/pilot_suppression_trial.html)


## Project structure

- `data/`: Original data files, as they were received. 

    I don't touch them at all. I may even set read-only permissions on them. I typically track versions (even if sometimes they are binary files) for reproducibility.

- `doc/`: Supporting documents, papers, etc. that are not produced by me, but are useful or needed for reference.

    Binary documents in PDF or doc(x) formats are untracked (edit `doc/.gitignore` as needed). However there is a `Notes.md` for keeping a project log and a `Makefile` to compile the notes to PDF format.
    
- `public/`: Dissemination material in HTML format (reports, slides). Everything here will be published automatically on line by _GitLab Pages_.

- `src/`: Source code for everything. Functions, figures, reports, analyses. 

    The `.Rmd` files produce RMarkdown reports (compiled into `public/`).
    
    Helper functions are in `functions.R`. The rest are ancilliary files. Write \LaTeX macros into `before_body.tex` and specific \LaTeX packages into `preamble.tex`. Include bibliographic references in BibTeX format into the `.bib` file. You can change the bibliographic style in the `.csl` file by whatever style you prefer.

- `.gitignore` specifies which files are not to be tracked.

- `.gitlab-ci.yml` configures GitLab's continuous integration in order to produce GitLab Pages based from `README.md` and files within `public/`.

- `README.md` is the current file. It will become the index page of the GitLab Pages website, and can include links to compiled reports for dissemination.

- `_targets.R` is the __main file of the project__. It consists of a list of _targets_ that need to be computed using functions and code therein. These _targets_ are R objects than can be referred to from within the `Rmd` files to produce the reports.

    Some of the targets load or clean up the data sets producing intermediate objects that are used by others in turn, others make figures and others are meant to compile the reports themselves.
