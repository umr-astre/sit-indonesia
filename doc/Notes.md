## Meeting 2022-08-22

- During the suppression study, adult sterile males __were not marked__ (despite
the statement in the protocol). So, not possible to compute sterile/wild ratio
accurately (can be approximated by the sex ratio in comparison with a control
area.)


- Which results are required from the analysis?

  The sections that I sketched in the report are ok, except for a couple of 
  those that are not possible to compute due to the lack of point releases.
  That has been noted in the report.
  Also, check Martín-Park et al. 2022 for inspiration.


- Protocol intro: "Monitoring system for entomological data in the selected
field sites has been established using ovitrap surveillance". But there is adult
surveillance as well, no?

- Marked sterile males are released using fluorescent dust and Rhodamine B as visual markers. Only one colour? Only one line-release? When?

  No, we don't have marks. Releases were performed on Wednesdays.
  The first week of each month is the week that starting on Wednesday contains
  the first day of the month.


- Are there coordinates for traps or should they be rather pooled by site?

  Yes there are. They will send them.
  

- The suppression protocol reports the surface area of the release site and the
main control site, but not of the two additional control sites.

  Will send later.


- How to treat the relatively many ovitraps that are in-between sites? (See Fig.
1 in the protocol). The areas in Fig. 1 were drown by hand in Word. Am I seeing
them correctly?

    Exclude.


- Sterile male releases: how many? Is this the 12 release records in "No of male released.xlsx"? which days in the week?

  Yes, Wednesdays.


- ovitraps GPS coordinates? (protocol says they were collected)

  Yes. Send later.


- Protocol: "The ODI is the average number of _Aedes_ eggs per _positive ovitrap_, while the OI was determined by dividing the number of ovitraps containing _Aedes_ egg or immature mosquitoes by the total number of ovitraps observed"

   - What is a _positive ovitrap_ in the protocol, when talking about ovitrap density index (ODI)?

      positive ovitrap: an ovitrap with at least 1 egg. Check out paper of Cuba trial.
   
   - ODI and OI ("No of Eggs.xlsx") are computed across ovitraps. Can these
   results be reproduced from data in "Hatching rate.xlsx"?

      In principle, yes. But they are checking the totals, which don't match.
      But then, no. They won't ever match.  There is no way to have exactly the
      same total number of eggs in “no of eggs” and “hatching rate”. Data in “no
      of eggs” were collected directly, on site by the data collectors. But
      total in “hatching rate”, we counted the eggs on tissue paper (oviposition
      substrate) that had been processed (dried, packed, transported, hatched,
      counted), so loss of eggs are expected.


- In "Hatching Rate.xlsx", the highlighted cells carry any information? 
    
    No.

- In "No of Eggs.xlsx", variable Week ranges from 9 to 52 and then it starts
over from 1 to 13. Am I right to interpret this as weeks in the next year?

  Yes. In the summary is more explicit.



## Further clarifications about the data

The documentation of the data is provided in the file "Excel File Description_Indonesia.docx".
Further questions issued by e-mail and their answers are summarised below.


### Released males

1. Just to confirm that the "Dates" are expressed as Month dash Week-number-within-month, whereas Week is the week number in the year, right? Is the only alternative that I can think  of that makes any sense. But since this is not explicit, I prefer to confirm. I might miss something.

  - You are right. Nov-01 means the first week in November that also means 45th week in 2021. 

2. I don't understand what variables Day1, Day2, Day3 represent. Are these 3 different releases in the week. If so, which days exactly? Your description says "sterile male production from day 1 - 3 pupal harvest". But I don't understand what that means. Is this related to the production or to the release of sterile males?

  - Day 1, Day 2 and Day 3 related to the production of male pupae. All pupae from these days were irradiated and released at once. The total column is number of male released, which is a sum of pupal production minus mortality (number of death/nonflyer mosquito).

3. What does "n.a." means under "Mortality"? Is it unknown, 0, or not applicable for some reason?

  - It was unknown because we didn’t collect the data at the first and second release.


4. What were the release points? or were they all line-releases?

  - Yes, they were ground line-releases.


### BG trap collection

1. Just to confirm. File no. 3 was "Hatching rate.xlsx". I guess this is a mistake and you meant file no. 4: "Adult surveillance_2.xlsx". Right?

   I am sorry, my bad. I confirm that the file refer to file No. 4 - “Adult surveillance_2.xlsx”


2. I understand that these are aggregated numbers of AE across all 10 BG traps in the release and control sites. However, this does not match with the original data from the sheets "Control Site 1" and "Release Site" in the file "Adult surveillance_2.xlsx". Actually the data from the release and control sites seem switched, except for the 60 females in the Release Site at Jun-02, which on the detailed file are 65. Please confirm this mistake, and that the detailed file is more accurate than the summary file.

  For this matter, I put the wrong sheet’s name in, yes they are switched. To confirm 65/60 detailed data, I must see my log book. I will come to you tomorrow.
  I can confirm that the right number of females captured in week 2 of June was 65, not 60.



3. In the description, you say that "only release and control site 1 are included". Are there more than one control sites?

  Yes, the are 2 other additional control sites, in which BG traps were not installed. We only did ovitrap surveillance for control site 2 and control site 3.


### Study description

1. What do "MRR 1" ... "MRR 3" stand for? Are these 3 different releases?

2. What is the relationship, if any, with the data in the file "No of male released.xlsx"? The number of individuals released in the former (7500, 12000, 15000) have nothing to do with the numbers released in the latter file.

  MRR stands for Mark-Release-Recapture. We performed three MRR events (MRR 1, MRR 2, MRR 3) and they have nothing to do with “Number of male released” file. MRR and open release (corresponds to the file no. 1, 2, 3, 4, and 5 written in the description doc) were two completely different activities. 




## Some lessons on data collection and organisation

- Share only raw data. No summaries, nor re-structured versions of the original data. See mistakes of switching columns and mistaken data caused by this.

- Organise data in single tables, with one header. No small-multiples. No multiple-headers. No creative visualisations.

- Do not rely on row ordering to encode information. Use explicit variables. See
how the year is implicitly encoded in the ordering in "No of Eggs.xlsx".

- Separate data from calculations. "Both sexes", "sums" and "percentages" are all calculations. Not raw data.

- Write dates in a explicit way. Preferably using the ISO-8601 standard (yyyy-mm-dd). "Jun-01" is ambiguous (first week or day?) and you have to pick the year from somewhere else. Even the first week of june is ambiguous (is it the week to which the 1st of june belongs, or the first week entirely within june? does the week start on Monday or on Sunday?). Just write the date. If you don't know the day, write a column for the month and a column for the week within the month, or within the year.

- Require data documentation and a description of the study protocols.
